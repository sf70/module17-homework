const sum = require('../../17.5.1/1');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});