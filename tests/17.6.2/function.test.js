import { getMonth } from "../../17.6.2/function";

describe("tests for getMonth function", () => {
  const num1 = 5;
  const num2 = 100;
  const str = 'test';

  it("should reverse string", () => {
    expect(getMonth(str)).toBe('value type error, need number');
    expect(getMonth(num1)).toBe('май');
    expect(getMonth(num2)).toBe('month number error');
  });
});